package com.hydra.sushchak.hushcha.hydra;

import android.util.Log;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Tu odbywa się realizacja metod  po otrzymaniu oraz ustawieniu informacji o zdjęciach
 */
public class ImageData{

    private String name;
    private String date;
    private String size;
    private String photo;

    public boolean isPdf() {
        return pdf;
    }
    public ImageData(){}

    public ImageData(File file){
        setFilePath(file.getAbsolutePath());
        setName(file.getName().substring(0, file.getName().length() - 4));
        double size = ((double) file.length()) / 1000000;
        setSize(new DecimalFormat("#.##").format(size) + " MB");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Log.d("...", sdf.format(file.lastModified()));

        setDate(sdf.format(file.lastModified()));
    }

    private boolean pdf;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSize() { return size; }


    public void setSize(String size) {
        this.size = size;
    }

    public String getFilePath() {
        return photo;
    }

    public void setFilePath(String filePath) {

        this.photo = filePath;
        String type = filePath.substring(filePath.length() - 3);
        if(type.equals("pdf"))
            pdf = true;
        else pdf = false;
    }

    @Override
    public String toString() {
        return "[ name =" + name + ", date=" + date + ", size=" + size;
    }

}
