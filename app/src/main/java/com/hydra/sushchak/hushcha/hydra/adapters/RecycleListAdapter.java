package com.hydra.sushchak.hushcha.hydra.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hydra.sushchak.hushcha.hydra.ConstForIntent;
import com.hydra.sushchak.hushcha.hydra.ImageData;
import com.hydra.sushchak.hushcha.hydra.Pliki;
import com.hydra.sushchak.hushcha.hydra.R;
import com.hydra.sushchak.hushcha.hydra.activities.FilterActivity;
import com.hydra.sushchak.hushcha.hydra.activities.SliderImageActivity;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Klasa realizująca listę filtrów do wyboru w FilterActivity
 */
public class RecycleListAdapter extends RecyclerView.Adapter<RecycleListAdapter.ViewHolder> implements ConstForIntent {

    /** lista danych do wyświetlenia */
    private ArrayList<ImageData> galleryList;

    private Context context;

    public RecycleListAdapter(Context context, ArrayList<ImageData> galleryList) {
        this.galleryList = galleryList;
        this.context = context;
    }

    @Override
    public RecycleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecycleListAdapter.ViewHolder holder, final int position) {

        if(!galleryList.get(position).isPdf())
            Glide.with(context)
                    .load(new File(galleryList.get(position).getFilePath()))
                    .into(holder.photo);
        else holder.photo.setImageResource(R.drawable.ic_picture_as_pdf_black_24dp);

        holder.name.setText(galleryList.get(position).getName());
        holder.date.setText(galleryList.get(position).getDate());
        holder.size.setText(galleryList.get(position).getSize());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!galleryList.get(position).isPdf()) {
                    Intent intent = new Intent(context, SliderImageActivity.class);
                    intent.putExtra(ConstForIntent.IMAGE_POSITION, position);
                    context.startActivity(intent);
                }else{
                    File file = new File(galleryList.get(position).getFilePath());
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    context.startActivity(intent);
                }
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                showPopupMenu(holder.itemView, position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView photo;
        private TextView name;
        private TextView date;
        private TextView size;

        public ViewHolder(View itemView) {
            super(itemView);
            photo = (ImageView) itemView.findViewById(R.id.iv_Photo);
            name = (TextView) itemView.findViewById(R.id.tvNamePhoto);
            date = (TextView) itemView.findViewById(R.id.tvDatePhoto);
            size = (TextView) itemView.findViewById(R.id.tvSizePhoto);
        }
    }

    private void showPopupMenu(final View view, final int position){
        PopupMenu popupMenu = new PopupMenu(context, view);
        popupMenu.inflate(R.menu.popup_menu);
        Menu menu = popupMenu.getMenu();
        if(galleryList.get(position).isPdf()) {
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(false);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.item_create_pdf: {
                        final ProgressDialog pd = new ProgressDialog(context);
                        pd.setTitle("Creating PDF");
                        pd.show();
                        final Pliki plik = new Pliki();
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    plik.saveAsPdf(galleryList.get(position).getFilePath(), "PDF_" + galleryList.get(position).getName(), 0);
                                    pd.dismiss();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (DocumentException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                        return true;
                    }
                    case R.id.item_delete: {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                        dialog.setMessage(R.string.dialog_message_remove);
                        dialog.setPositiveButton(context.getString(R.string.dialog_positive_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Pliki pliki = new Pliki();
                                if (pliki.deleteFile(galleryList.get(position).getFilePath())) {
                                    galleryList.remove(position);
                                    notifyDataSetChanged();
                                    Toast.makeText(context, context.getString(R.string.toast_image_deleted), Toast.LENGTH_SHORT).show();
                                } else
                                    Toast.makeText(context, context.getString(R.string.toast_image_not_deleted), Toast.LENGTH_SHORT).show();

                            }
                        });
                        dialog.setNegativeButton(R.string.dialog_negative_button, null);
                        dialog.show();
                        return true;
                    }
                    case R.id.item_edit: {
                        Intent intent = new Intent(context, FilterActivity.class);
                        intent.putExtra(ConstForIntent.IMAGE_DESTINATION, galleryList.get(position).getFilePath());
                        context.startActivity(intent);
                        return true;
                    }

                }
                return false;
            }
        });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu popupMenu) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {  // center popup menu
            popupMenu.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL);
            Log.d("...", "center horizontal popup_menu" );
        }
        popupMenu.show();
    }
}