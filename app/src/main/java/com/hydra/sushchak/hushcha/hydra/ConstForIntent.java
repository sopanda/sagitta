package com.hydra.sushchak.hushcha.hydra;


/**
 * Interfejs przechowujący klucze do transmisji danych pomiędzy activity.
 */
public interface ConstForIntent {
    String IMAGE_POSITION = "ImagePosition";
    String IMAGE_DESTINATION = "Image_destination";
}
