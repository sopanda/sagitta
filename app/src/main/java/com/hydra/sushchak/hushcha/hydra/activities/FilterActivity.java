package com.hydra.sushchak.hushcha.hydra.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hydra.sushchak.hushcha.hydra.ConstForIntent;
import com.hydra.sushchak.hushcha.hydra.Pliki;
import com.hydra.sushchak.hushcha.hydra.adapters.AdapterFilterView;
import com.hydra.sushchak.hushcha.hydra.R;

import java.io.IOException;

/**
 * Klasa realizująca okno wyboru filtrów.
 */
public class FilterActivity extends AppCompatActivity implements ConstForIntent {

    private RecyclerView mRecyclerView;

    /** adapter dla wyświetlania */
    private AdapterFilterView mAdapter;

    private RecyclerView.LayoutManager mLayoutManager;

    /** kontrolka progresu dla nałożeniu filtra*/
    private ProgressBar progressBar;

    /** mapa bitów obrazku*/
    private Bitmap bitmap;

    /** kontrolka do wyświetlania zdjęcia*/
    private ImageView image;

    /** miejsce zachowania obrazku */
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        String[] myDataset = getResources().getStringArray(R.array.filterName);

        Intent intent = getIntent();
        if(intent != null) {
            filePath = intent.getStringExtra(IMAGE_DESTINATION);
            bitmap = BitmapFactory.decodeFile(filePath);
        }

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mRecyclerView = (RecyclerView) findViewById(R.id.lvFilterList);
        image = (ImageView) findViewById(R.id.ivPhoto);

        // если мы уверены, что изменения в контенте не изменят размер layout-а RecyclerView
        // передаем параметр true - это увеличивает производительность
        mRecyclerView.setHasFixedSize(true);

        // используем linear layout manager
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // создаем адаптер
        mAdapter = new AdapterFilterView(myDataset, bitmap, image, progressBar, this);
        mRecyclerView.setAdapter(mAdapter);

        image.setImageBitmap(bitmap);
    }


    @Override
    protected void onDestroy() {
        mAdapter.getTask().cancel(true);
        mAdapter.getTaskForFilters().cancel(true);
        super.onDestroy();
    }

    /**
     * metoda obsługi zdarzeń
     * @param view wciśnięta kontrolka
     */
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ibtnSave: {
                Pliki pliki = new Pliki();
                Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
                try {
                    pliki.saveImage(bitmap, filePath);
                    onBackPressed();
                } catch (IOException e) {
                    Toast.makeText(this, getString(R.string.toast_image_not_save), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.ibtnRotate:{
                Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
                bitmap = rotateImage(bitmap);
                image.setImageBitmap(bitmap);
                mAdapter.setBitmap(bitmap);
                mAdapter.notifyDataSetChanged();
                break;
            }
            case R.id.ibtnBack:{
                onBackPressed();
                break;
            }
        }
    }

    /**
     * funkcja obrótu zdjęc na 90 stopni
     * @param bitmap  zdjęcie do obrótu
     * @return zwraca obrócone zdjęcie
     */
    private Bitmap rotateImage(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}