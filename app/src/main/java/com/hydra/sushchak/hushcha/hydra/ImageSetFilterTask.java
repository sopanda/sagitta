package com.hydra.sushchak.hushcha.hydra;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.lang.ref.WeakReference;

/**
 * Klasa realizuje interakcje z filtrami i pomaga zwiększyć wydajność przy nałożeniu filtrów.
 */
public class ImageSetFilterTask extends AsyncTask<Integer, Void, Bitmap> {

    /** referencja do kontrolki dla zdjęcia*/
    private final WeakReference<ImageView> imageViewReference;

    /** zdjęcie do edytowania*/
    private Bitmap bitmap;

    /** referencja do kontrolki progresu */
    private final WeakReference<ProgressBar> progressBarWeakReference;

    public ImageSetFilterTask(ImageView imageView, ProgressBar progressBar, Bitmap bitmap) {
        imageViewReference = new WeakReference<ImageView>(imageView);
        if(progressBar != null)
            progressBarWeakReference = new WeakReference<ProgressBar>(progressBar);
        else progressBarWeakReference = null;
        this.bitmap = bitmap;
    }

    @Override
    protected void onPreExecute() {
        if (progressBarWeakReference != null)
        {
            ProgressBar progressBar = progressBarWeakReference.get();
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Bitmap doInBackground(Integer... numFilter) {
        return setFilter(numFilter[0], bitmap);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }
        if (imageViewReference != null) {
            ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    imageView.setImageResource(R.drawable.someimage);
                }
            }
        }
        if (progressBarWeakReference != null)
        {
            ProgressBar progressBar = progressBarWeakReference.get();
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * metoda ustawienia filtra na zdjęcie
     * @param numFilter numer filtra
     * @param bitmap zdjęcie
     * @return zwraca zmienionie zdjęcie
     */
    public Bitmap setFilter(int numFilter, Bitmap bitmap){
        Filters filters = new Filters();

        switch (numFilter){
            case 0:
                //bitmap = filters.setInvertEffect(bitmap);
                break;
            case 1:
                bitmap = filters.setInvertEffect(bitmap);
                break;
            case 2:
                bitmap = filters.setGreyscaleEffect(bitmap);
                break;
            case 3:
                bitmap = filters.setGammaEffect(bitmap, 1.8, 1.8, 1.8);
                break;
            case 4:
                bitmap = filters.setColorFilterEffect(bitmap, 0.1, 0.4, 0.2);
                break;
            case 5:
                bitmap = filters.setSepiaToningEffect(bitmap, 2, 0.3, 0.1, 0.3);
                break;
            case 6:
                bitmap = filters.setMeanRemovalEffect(bitmap);
                break;
            case 7:
                bitmap = filters.setSmoothEffect(bitmap, 2);
                break;
            case 8:
                bitmap = filters.setFleaEffect(bitmap);
                break;
            case 9:
                bitmap = filters.setBlackFilter(bitmap);
                break;
            case 10:
                bitmap = filters.setSnowEffect(bitmap);
                break;
            case 11:
                bitmap = filters.setShadingFilter(bitmap, Color.MAGENTA);
                break;
        }
        return bitmap;
    }
}