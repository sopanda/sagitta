package com.hydra.sushchak.hushcha.hydra;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *  Klasa realizuje tworzenie foldera na SD karcie oraz
 *  zapisuję u niego zrobione zdjęcia. Tu odbywa się sapis
 *  zdjęcia w PDF oraz realizowana możliwość usunięcia zdjęć.
 */
public class Pliki {

    private final String TAG = "myLogs";

    /** teczka zachowania dla zdjęć */
    private final String DIR_PHOTO = "Hydra/MyPhotos";

    /**teczka zachowania dla plików */
    private final String DIR_DOCUMENT = "Hydra/MyDocuments";

    public Pliki(Context context)
    {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Log.d(TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }
        File saveDir = new File(Environment.getExternalStorageDirectory()  //create folder for photo
                .getPath() + File.separator + DIR_PHOTO + File.separator);
        if(!saveDir.exists()) saveDir.mkdirs();

        File saveDoc = new File(Environment.getExternalStorageDirectory() // create folder for documents
                .getPath() + File.separator + DIR_DOCUMENT + File.separator);
        if(!saveDoc.exists()) saveDoc.mkdirs();
    }

    public Pliki()
    {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Log.d(TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }
        File saveDir = new File(Environment.getExternalStorageDirectory()  //create folder for photo
                .getPath() + File.separator + DIR_PHOTO + File.separator);
        if(!saveDir.exists()) saveDir.mkdirs();

        File saveDoc = new File(Environment.getExternalStorageDirectory() // create folder for documents
                .getPath() + File.separator + DIR_DOCUMENT + File.separator);
        if(!saveDoc.exists()) saveDoc.mkdirs();
    }



    public void savePhoto(Bitmap bitmap, String fileName) throws IOException {

        if(fileName == null) {
            String imageName = "IMG_" + System.currentTimeMillis();
            fileName = String.format(Environment.getExternalStorageDirectory()
                            .getPath() + File.separator + DIR_PHOTO + "/%s.jpg",
                    imageName) ;
        } else {
            fileName = String.format(Environment.getExternalStorageDirectory()
                    .getPath() + File.separator + DIR_PHOTO , "/%d.jpg", fileName);
        }

        FileOutputStream fOut = new FileOutputStream(fileName);

        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        bitmap = Bitmap.createScaledBitmap(bitmap, 1280, 720, false);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        Log.d("...", bitmap.getHeight() + " / " + bitmap.getWidth());

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);

        fOut.flush();
        fOut.close();

    }

    public void saveImage(Bitmap bitmap, String filePath) throws IOException {
        if(filePath == null) {
            String imageName = "IMG_" + System.currentTimeMillis();
            filePath = String.format(Environment.getExternalStorageDirectory()
                            .getPath() + File.separator + DIR_PHOTO + "/%s.jpg",
                    imageName) ;
        } else {
            File image;
            do{
                filePath = filePath.substring(0, filePath.length() - 4) + "f.jpg";
                image = new File(filePath);
            }while (image.exists());

            //filePath = String.format(Environment.getExternalStorageDirectory()
            //      .getPath() + File.separator + DIR_PHOTO , "/%d.jpg", filePath);
        }





        FileOutputStream fOut = new FileOutputStream(filePath);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
        fOut.flush();
        fOut.close();
    }

    public File[] readImages(){
        File folder = new File(Environment.getExternalStorageDirectory().getPath()
                + File.separator + DIR_PHOTO);
        if(folder.exists())
            return folder.listFiles();
        else {
            //folder.mkdirs();
            return null;
        }
    }

    public File[] readDocuments(){
        File folder = new File(Environment.getExternalStorageDirectory().getPath()
                + File.separator + DIR_DOCUMENT);
        if(folder.exists())
            return folder.listFiles();
        else {
            //folder.mkdirs();
            return null;
        }
    }

    public File[] readAllFiles(){
        File imageFolder = new File(Environment.getExternalStorageDirectory().getPath()
                + File.separator + DIR_PHOTO);

        File documentFolder = new File(Environment.getExternalStorageDirectory().getPath()
                + File.separator + DIR_DOCUMENT);

        File[] images = null;
        if(imageFolder.exists())
            images = imageFolder.listFiles();

        File[] documents = null;
        if(documentFolder.exists())
            documents = documentFolder.listFiles();

        File[] list = null;
        try {
            list = new File[images.length + documents.length];
        }
        catch (NullPointerException e){
            if(images == null)
                return documents;

            else if(documents == null)
                return images;
        }
        int ixImg = 0;
        int ixDoc = 0;
        for(int i = 0; i < list.length; i++)
            if(ixImg < images.length) {
                list[i] = images[ixImg];
                ++ixImg;
            }
            else {
                list[i] = documents[ixDoc];
                ++ixDoc;
            }

        return list;
    }

    public void saveAsPdf(String imagePath, String namePDF, float rotationDegrees) throws IOException, DocumentException {

        Document document = new Document();
        String dirpath = Environment.getExternalStorageDirectory().getPath()
                + File.separator + DIR_DOCUMENT;
        String filePath = dirpath + File.separator + namePDF + ".pdf";
        File file;
        do{
            filePath = filePath.substring(0, filePath.length() - 4) + "copy.pdf";
            file = new File(filePath);
        }while (file.exists());

        PdfWriter.getInstance(document, new FileOutputStream(filePath));
        document.open();
        Image img = Image.getInstance(imagePath);

        img.setRotationDegrees(rotationDegrees);
        img.setAlignment(Image.ALIGN_CENTER| Image.TEXTWRAP);

        float width = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
        float height = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();
        img.scaleToFit(width, height);

        document.add(img);
        document.close();
    }

    public void saveAsPdf(String []imagesPath, String NamePDF) throws IOException, DocumentException {
        Document document = new Document();
        String dirpath = Environment.getExternalStorageDirectory().getPath()
                + File.separator + DIR_DOCUMENT;

        PdfWriter.getInstance(document, new FileOutputStream(dirpath + File.separator + NamePDF + ".pdf"));
        document.open();

        for(String path : imagesPath) {
            Image img = Image.getInstance(path);  // Change image's name and extension.

            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / img.getWidth()) * 100;
            img.scalePercent(scaler);
            img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            document.add(img);
        }
        document.close();

    }

    public File createTemporaryFile(String part, String ext) throws Exception
    {
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public boolean deleteFile(String filePath){
        File file = new File(filePath);
        if (file.exists())
            return file.delete();
        return false;
    }

}