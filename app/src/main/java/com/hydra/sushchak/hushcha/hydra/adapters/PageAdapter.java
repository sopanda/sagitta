package com.hydra.sushchak.hushcha.hydra.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hydra.sushchak.hushcha.hydra.R;

import java.io.File;

/**
 *  Klasa realizująca przeglądanie zdjęć na cały ekran smartfonu
 */
public class PageAdapter extends PagerAdapter {
    private File[] images;
    private Context context;
    private LayoutInflater layoutInflater;


    public PageAdapter(File[] images, Context context) {
        this.images = images;
        this.context = context;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_image_full, container, false);
        final ImageView imageView = (ImageView) view.findViewById(R.id.imageFull);
        Glide.with(context).load(images[position]).into(imageView);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }

}
