package com.hydra.sushchak.hushcha.hydra.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;
import com.hydra.sushchak.hushcha.hydra.ImageSetFilterTask;
import com.hydra.sushchak.hushcha.hydra.R;


/**
 * Klasa realizująca preview filtrowanego zdjęcia w liście proponowanych filtrów
 */
public class AdapterFilterView extends RecyclerView.Adapter<AdapterFilterView.ViewHolder>{
    private String[] mDataset;
    private Bitmap bitmap;
    private Bitmap bitmapSmall;
    private ImageView imageView;
    private int currentFilter = -1;

    private Context context;
    private ProgressBar progressBar;

    private ImageSetFilterTask taskForFilters;
    private ImageSetFilterTask task;


    // класс view holder-а с помощью которого мы получаем ссылку на каждый элемент
    // отдельного пункта списка
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public ImageView mImageView;

        public ViewHolder(View v){
            super(v);
            mTextView = (TextView) v.findViewById(R.id.ourTextView);
            mImageView = (ImageView) v.findViewById(R.id.ourImageView);
        }
    }

    // Конструктор
    public AdapterFilterView(String[] dataset, Bitmap bitmap ,ImageView imageView, ProgressBar progressBar, Context context) {
        this.mDataset = dataset;
        this.bitmap = bitmap;
        this.imageView = imageView;
        this.bitmapSmall = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
        this.task = new ImageSetFilterTask(imageView, progressBar, bitmap);
        this.context = context;
        this.progressBar = progressBar;

    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public AdapterFilterView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_filter, parent, false);

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    // Заменяет контент отдельного view (вызывается layout manager-ом)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.mTextView.setText(mDataset[position]);

        if(holder.mImageView != null) {
            taskForFilters = new ImageSetFilterTask(holder.mImageView, null, bitmapSmall);
            taskForFilters.execute(position);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentFilter != position) {
                    if (task.getStatus() == AsyncTask.Status.RUNNING) {
                        task.cancel(false);
                        task = new ImageSetFilterTask(imageView, progressBar, bitmap);
                        task.execute(position);
                        currentFilter = position;
                        Toast.makeText(context, task.getStatus().toString(), Toast.LENGTH_SHORT).show();
                    } else if (task.getStatus() == AsyncTask.Status.PENDING || task.getStatus() == AsyncTask.Status.FINISHED) {
                        task = new ImageSetFilterTask(imageView, progressBar, bitmap);
                        task.execute(position);
                        currentFilter = position;
                        Toast.makeText(context, task.getStatus().toString(), Toast.LENGTH_SHORT).show();
                    }
                }else if (currentFilter == -1) {
                    task.execute(position);
                    currentFilter = position;
                    Toast.makeText(context, task.getStatus().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    public ImageSetFilterTask getTask(){
        return task;
    }
    public ImageSetFilterTask getTaskForFilters(){
        return taskForFilters;
    }
    public void setBitmap(Bitmap bitmap)
    {
        this.bitmap = bitmap;
        this.bitmapSmall = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }


}
