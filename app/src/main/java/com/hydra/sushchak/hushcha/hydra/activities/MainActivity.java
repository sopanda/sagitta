package com.hydra.sushchak.hushcha.hydra.activities;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hydra.sushchak.hushcha.hydra.ImageData;
import com.hydra.sushchak.hushcha.hydra.Pliki;
import com.hydra.sushchak.hushcha.hydra.R;
import com.hydra.sushchak.hushcha.hydra.adapters.RecycleListAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 *  Klasa realizująca główne okno aplikacji oraz
 *  do wczytywania wszyskich plików aktualnej dyrektorii.
 */
public class MainActivity extends AppCompatActivity {

    public final int REQUEST_CHOOSE_PHOTO = 2;
    public final int REQUEST_CAP_PHOTO = 1;

    protected Uri mImageUri;
    private ArrayList<ImageData> listItems;
    private RecycleListAdapter adapter;
    private RecyclerView recyclerView;

    /** enum dla wcisnięciej kontrolki */
    private enum CheckedItem {IMAGES, DOCUMENTS, IMAGES_AND_DOCUMENTS}

    private CheckedItem checkedItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.imagegallery);
        recyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        checkedItem = CheckedItem.IMAGES;
    }

    /**
     * metoda która przyjmuje listę plików
     * @param files lista plików
     * @return listMockData lista wczytanych plików
     */
    private ArrayList<ImageData> getListData(File[] files){
        ArrayList<ImageData> listMockData = new ArrayList<>();
        if(files != null)
            for (File file : files) {
                ImageData newsData = new ImageData(file);
                listMockData.add(newsData);
            }
        return listMockData;
    }

    @Override
    protected void onResume() {
        switch (checkedItem){
            case IMAGES:
                listImages();
                break;
            case DOCUMENTS:
                listDocuments();
                break;
            case IMAGES_AND_DOCUMENTS:
                listImageAndDocuments();
                break;
        }
        super.onResume();
    }

    /**
     * funkcja która obsługuje zdarzenia dla wywolania odpowiedniej metody do wyświetlania
     * obrazków, plików pdf lub wsystkich razem
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_list_images:
                    listImages();
                    checkedItem = CheckedItem.IMAGES;
                    return true;
                case R.id.navigation_list_documents:
                    listDocuments();
                    checkedItem = CheckedItem.DOCUMENTS;
                    return true;
                case R.id.navigation_list_documents_and_images:
                    listImageAndDocuments();
                    checkedItem = CheckedItem.IMAGES_AND_DOCUMENTS;
                    return true;
            }
            return false;
        }

    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * metoda do ustawienia adaptera dla wszystkich obrazków
     */
    private void listImages(){
        listItems = getListData(new Pliki(this).readImages());
        adapter = new RecycleListAdapter(MainActivity.this, listItems);
        recyclerView.setAdapter(adapter);
    }

    /**
     * metoda do ustawienia adaptera dla wszystkich plików PDF
     */
    private void listDocuments(){
        listItems = getListData(new Pliki(this).readDocuments());
        adapter = new RecycleListAdapter(MainActivity.this, listItems);
        recyclerView.setAdapter(adapter);
    }

    /**
     * metoda do ustawienia adaptera dla wszystkich obrazków oraz plików PDF
     */
    private void listImageAndDocuments(){
        listItems = getListData(new Pliki(this).readAllFiles());
        adapter = new RecycleListAdapter(MainActivity.this, listItems);
        recyclerView.setAdapter(adapter);
    }

    /**
     * metoda dla obsługi zdarzeń dla menu
     * @param item wybrany punkt menu
     * @return zwraca wybrany item menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.item_plus:
                MainDialog dialog = new MainDialog();
                dialog.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap;

        if(requestCode == REQUEST_CHOOSE_PHOTO && resultCode == RESULT_OK){
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                Pliki image = new Pliki();
                image.savePhoto(bitmap, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(requestCode == REQUEST_CAP_PHOTO && resultCode == RESULT_OK){
            ContentResolver cr = this.getContentResolver();
            try {

                Pliki document = new Pliki();
                bitmap = MediaStore.Images.Media.getBitmap(cr, mImageUri);
                document.savePhoto(bitmap, null);

            }catch (IOException e){
                e.printStackTrace();

            } catch (Exception e) {
                Toast.makeText(MainActivity.this, "Please check SD card! ImageData shot is impossible!", Toast.LENGTH_SHORT).show();
            }
            adapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * wlożona klasa realizująca wybór akcji użytkownika.
     */
    private class MainDialog implements View.OnClickListener{

        private Dialog dialog;
        private ImageView ivTakePhoto;
        private ImageView ivChoosePhoto;
        private TextView tvTakePhoto;
        private TextView tvChoosePhoto;

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.tvTakePhoto:
                    takeFoto();
                    break;
                case R.id.ivTakePhoto:
                    takeFoto();
                    break;
                case R.id.tvChoosePhoto:
                    chosePhoto();
                    break;
                case R.id.ivChoosePhoto:
                    chosePhoto();
                    break;
            }
        }

        private void takeFoto(){
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            File photo = null;
            try {
                photo = new Pliki(MainActivity.this).createTemporaryFile("picture", ".jpg");
            } catch (Exception e) {
                e.printStackTrace();
            }
            photo.delete();
            mImageUri = Uri.fromFile(photo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            dialog.cancel();
            MainActivity.this.startActivityForResult(intent, REQUEST_CAP_PHOTO);
        }
        private void chosePhoto(){
            Intent intent2 = new Intent(Intent.ACTION_PICK);
            intent2.setType("image/*");
            dialog.cancel();
            MainActivity.this.startActivityForResult(intent2, REQUEST_CHOOSE_PHOTO);
        }

        public MainDialog(){
            init();
        }
        /**
         * metoda do inicializacji wszystkich zmienych dialogu
         */
        public void init(){
            dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.main_dialog);
            dialog.setTitle(R.string.title_dialog);

            ivChoosePhoto = (ImageView) dialog.findViewById(R.id.ivChoosePhoto);
            ivTakePhoto = (ImageView) dialog.findViewById(R.id.ivTakePhoto);

            tvTakePhoto = (TextView) dialog.findViewById(R.id.tvTakePhoto);
            tvChoosePhoto = (TextView) dialog.findViewById(R.id.tvChoosePhoto);

            ivChoosePhoto.setOnClickListener(this);
            ivTakePhoto.setOnClickListener(this);
            tvChoosePhoto.setOnClickListener(this);
            tvTakePhoto.setOnClickListener(this);
        }

        /**
         * metoda zadaniem której jest wyświętlanie dialogu w oknie
         */
        public void show(){
            dialog.show();
        }

    }
}