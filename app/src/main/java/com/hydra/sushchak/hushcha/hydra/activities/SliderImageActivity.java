package com.hydra.sushchak.hushcha.hydra.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hydra.sushchak.hushcha.hydra.ConstForIntent;
import com.hydra.sushchak.hushcha.hydra.ImageData;
import com.hydra.sushchak.hushcha.hydra.Pliki;
import com.hydra.sushchak.hushcha.hydra.R;
import com.hydra.sushchak.hushcha.hydra.adapters.PageAdapter;
import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;

import static android.R.attr.breadCrumbShortTitle;
import static android.R.attr.cacheColorHint;
import static android.R.attr.startX;

/**
 * Klasa realizująca otwieranie zdjęć na cały ekran smartfonu
 */
public class SliderImageActivity extends AppCompatActivity implements ConstForIntent {

    private ViewPager viewPager;

    /** adapter do wyświetlania*/
    private PageAdapter adapter;
    private Pliki pliki;

    private int CLICK_ACTION_THRESHHOLD = 100;
    private float startX;
    private float startY;

    private static final int UI_ANIMATION_DELAY = 300;

    /** obiekt do zarządzania wątkami*/
    private final Handler mHideHandler = new Handler();


    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            viewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private boolean mVisible;

    /**
     * wątek do wyświetlania kontrolek
     */
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * wątek do chowania kontrolek
     */
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_image);
        init();

        Intent intent = getIntent();
        if (intent != null)
            viewPager.setCurrentItem(intent.getIntExtra(IMAGE_POSITION, 0));
    }

    /**
     * funkcja dla inicializowaniia wszystkich zmienych
     */
    void init() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        pliki = new Pliki();
        adapter = new PageAdapter(pliki.readImages(), this);
        viewPager.setAdapter(adapter);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = motionEvent.getX();
                        startY = motionEvent.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        float endX = motionEvent.getX();
                        float endY = motionEvent.getY();
                        if (isAClick(startX, endX, startY, endY)) {
                            toggle();
                            return true;
                        }
                        return false;
                }
                return false;
            }
        });
    }

    /**
     * metoda do uzypelnienia był klik czy nie
     * @param startX pozycja po X gdzie użytkownik dotknął ekranu
     * @param endX ozycja po X gdzie użytkownik odpuścił paleć z ekranu
     * @param startY pozycja po Y gdzie użytkownik dotknął ekranu
     * @param endY pozycja po Y gdzie użytkownik odpuścił paleć z ekranu
     * @return true jeżeli to był click
     */
    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        if (differenceX > CLICK_ACTION_THRESHHOLD  || differenceY > CLICK_ACTION_THRESHHOLD ) {
            return false;
        }
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    /**
     * metoda chowania kontrolek
     */
    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * metoda do wyświetlania kontrolek
     */
    @SuppressLint("InlinedApi")
    private void show() {
        viewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Harmonogramy połączenia hide(), anuluje wcześniej zaplanowane połączenia.
     @param delayMillis milisekundy
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    /**
     * obsluga zdarzeń
     * @param view kliknięcia kontrolka
     */
    public void click(View view) {
        final File[] files = pliki.readImages();
        final ImageData imageData = new ImageData(files[viewPager.getCurrentItem()]);
        final String imagePath = files[viewPager.getCurrentItem()].getPath();

        switch (view.getId()) {
            case R.id.ibtn_edit: {
                Intent intent = new Intent(this, FilterActivity.class);
                intent.putExtra(IMAGE_DESTINATION, imagePath);
                startActivity(intent);
                break;
            }
            case R.id.ibtn_picture_as_PDF: {
                final ProgressDialog pd = new ProgressDialog(this);
                pd.setTitle("Creating PDF");
                pd.show();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Pliki pliki = new Pliki();
                        try {
                            pliki.saveAsPdf(imageData.getFilePath(), "PDF" + imageData.getName(), 0);
                            pd.dismiss();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
                break;
            }
            case R.id.ibtn_delete: {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage(R.string.dialog_message_remove);
                dialog.setPositiveButton(getString(R.string.dialog_positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int curItem = viewPager.getCurrentItem();
                        pliki.deleteFile(imagePath);
                        File[] images = pliki.readImages();
                        //adapter.notifyDataSetChanged();
                        adapter = new PageAdapter(images, SliderImageActivity.this);
                        viewPager.setAdapter(adapter);
                        if (curItem == files.length - 1)
                            --curItem;
                        else if (curItem != 0 && curItem < files.length)
                            ++curItem;
                        viewPager.setCurrentItem(curItem);
                    }
                });
                dialog.setNegativeButton(R.string.dialog_negative_button, null);
                dialog.show();
                break;
            }
        }
    }
}